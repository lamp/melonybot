require('dotenv').config();
global.Discord = require("discord.js");
global.Akairo = require("discord-akairo");
global.fs = require("fs");
global.fetch = require("node-fetch");
global.THEME_COLOR = process.env.THEME_COLOR || "#696969";

class MelonyClient extends Akairo.AkairoClient {
	constructor() {
		super({ // akairo options
			ownerID: process.env.OWNER_ID
		}, { //discord.js options
			disableMentions: 'everyone'
		});
		this.commandHandler = new Akairo.CommandHandler(this, {
			directory: "commands",
			prefix: process.env.CMD_PREFIX || "m!"
		});
		this.commandHandler.loadAll();
		this.commandHandler.on("error", (error, message, command) => {
			message.reply(`An error occured!\n\`\`\`${error.stack}\`\`\``)
			console.log(`Error in command ${command && command.id}\n${error.stack}`)
		});
	}
}

var client = new MelonyClient();
client.login(process.env.DISCORD_TOKEN);
client.on("ready", () => console.log("ready"));


var SimpleTriggerSystem = require("./SimpleTriggerSystem")
client.triggerSystem = new SimpleTriggerSystem(client, "trigger-words.csv")
