let { MessageAttachment } = require('discord.js');
let { Command } = require('discord-akairo');
let GoogleImages = require('google-images');

let gi = new GoogleImages(process.env.GCSEID, process.env.GAPIK);

module.exports = class extends Command {
    constructor() {
        super('melonyv1', {
           aliases: ['oldmelony'] ,
           category: 'hidden',
           description: "older version of melony command that doesn't work very well because google images api gives very limited amount of results for some reason so there's a lot of repetition.",
           typing: true
        });
    }
    async exec(message) {
        let page = Math.floor( Math.random() * 10 ) + 1;
        console.log(`getting melony images from page ${page}`);
        var results = await gi.search("melony", {page});
        console.log("results", results);
        let resultNumber = Math.floor(Math.random() * results.length);
        let result = results[resultNumber];
        console.log(`picked result ${resultNumber}`, result);
        message.channel.send(new MessageAttachment(result.url, "melony.jpg"));
    }
}
