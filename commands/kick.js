module.exports = class extends Akairo.Command {
	constructor() {
		super("kick", {
			aliases: ["kick"],
			args: [{
				id:"user",
				type:"user"
			}],
            userPermissions: ['KICK_MEMBERS'],
		}).usage = "<user>"
	}
	async exec(message, args) {
		try {
			await user.kick()
			await message.react("👌")
		} catch(e) {
			await message.reply(e.message)
		}
	}
}