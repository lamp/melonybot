let { Command } = require("discord-akairo")

module.exports = class extends Command {
	constructor() {
		super("userinfo", {
			aliases: ["userinfo", "ui"],
			args: [
				{
					id: "target",
					type: "user"
				}
			],
			typing: true,
		}).usage = "[user]"
	}
	async exec(message, args) {
		let user = args.target || message.author;
		let embed = {
			title: `User info for ${user.username}`,
			thumbnail: {
				url: user.avatarURL({size:2048,format:'png'}) || user.defaultAvatarURL
			},
			color: THEME_COLOR,
			fields: [
				{
					name: "ID",
					value: user.id,
					//inline: true
				},
				{
					name: "Tag",
					value: user.tag,
					//inline: true
				},
				{
					name: "Avatar URL",
					value: user.avatarURL({
						size: 4096,
						format: 'png',
						dynamic: true
					}),
					//inline: true
				},
				{
					name: "Created At",
					value: user.createdAt.toString(),
					//inline: true
				}
			]
		}
		await message.channel.send({embed});
	}
}