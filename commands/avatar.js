let { MessageAttachment } = require("discord.js")
let { Command } = require("discord-akairo")

module.exports = class extends Command {
	constructor() {
		super("avatar", {
			aliases: ["avatar", "av"],
			args: [
				{
					id: "target",
					type: "user"
				}
			],
			description: "Return your or a user's avatar.",
			typing: true
		}).usage = "[user]"
	}
	async exec(message, args) {
		let user = args.target || message.author;
		await message.reply(
			`here is ${args.target ? `${args.target.username}'s` : `your`} avatar!`,
			new MessageAttachment(user.avatarURL({
				format: 'png',
				size: 2048,
				dynamic: true
			}) || user.defaultAvatarURL)
		);
	}
}