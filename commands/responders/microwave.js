let { Command } = require("discord-akairo")
let { MessageAttachment } = require('discord.js')

module.exports = class extends Command {
	constructor() {
		super("microwave", {
			regex: /^microwave$/i,
			description: "responds with the sound of a microwave",
			category: 'responders'
		})
	}
	async exec(message, args) {
		await message.channel.send("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm", new MessageAttachment("resources/microwave.mp3"))
	}
}