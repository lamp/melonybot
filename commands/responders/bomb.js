module.exports = class extends Akairo.Command {
	constructor() {
		super("bomb", {
			regex: /^bomb$/i,
			description: "Bot responds with an animated bomb emoji",
			category: "responders"
		})
	}
	async exec(message, args) {
		let bomb = await message.channel.send(":bomb:")
		await new Promise(r => setTimeout(r, 3000))
		await bomb.edit(":boom:")
	}
}