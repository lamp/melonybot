let { Command } = require("discord-akairo")

module.exports = class extends Command {
    constructor() {
        super("how", {
            regex: /^how$/i,
            category: "responders",
            description: "Reacts with emoji letters."
        })
    }
    async exec(message, args) {
        for (let o of "🇭🇴🇼") await message.react(o)
    }
}