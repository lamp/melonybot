module.exports = class extends Akairo.Command {
	constructor() {
		super("gold", {
			regex: /^gold$/i,
			description: "Bot reacts with reddit gold",
			category: "responders"
		})
	}
	async exec(message, args) {
		let emoji = message.guild.emojis.cache.find(x => x.name == "gold")
		if (!emoji) {
			try {
				emoji = await message.guild.emojis.create("resources/gold_512.png", "gold")
			} catch (error) {
				console.error(`could not create gold emoji: ${error.message}`)
			}
		}
		if (emoji) await message.react(emoji)
		else await message.channel.send(new Discord.MessageAttachment("resources/gold_48.png"))
	}
}