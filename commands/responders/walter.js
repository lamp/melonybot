let { Command } = require("discord-akairo")
let fs = require('fs').promises
let { MessageAttachment } = require('discord.js')

module.exports = class extends Command {
	constructor() {
		super("walter", {
			regex: /^walter$/i,
			description: "Send a random image of Walter the dog",
			category: "responders"
		})
	}
	async exec(message, args) {
        let dirlist = await fs.readdir("images/walters");
        let randomitem = dirlist[ Math.floor( Math.random() * dirlist.length ) ];
        await message.channel.send(new MessageAttachment(`images/walters/${randomitem}`));
	}
}