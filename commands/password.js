let { Command } = require("discord-akairo")

module.exports = class extends Command {
	constructor() {
		super("password", {
			aliases: ["password", "pw"],
			args: [
				{
					id: "pwlength",
					type: "number",
					default: 16
				}
			],
			description: "Generates a strong password with optional length.",
			typing: true
		}).usage = "[length]"
	}
	async exec(message, args) {
		let password = "";
		let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		if (args.pwlength > 1024) args.pwlength = 1024;
		for (let i = 0; i < args.pwlength; i++) {
			password += chars.charAt(Math.floor(Math.random() * chars.length));
		}
		try {
			await message.author.send(`\`${password}\``);
			await message.react("👌")
		} catch(e) {
			await message.reply(`I couldn't DM your password so here it is: \`${password}\``)
		}
	}
}