let { Command } = require("discord-akairo")

module.exports = class extends Command {
	constructor() {
		super("pd", {
			aliases: ["pd"],
			description: "Pings and dms the person in question with a message.",
			args: [
				{
					id: "person",
					type: "user"
				},
				{
					id: "message",
					match: "restContent"
				}
			],
			typing: true
		}).usage = "<user> <message...>"
	}
	async exec(message, args) {
		if (!args.person) return message.reply("You need to specify a valid user after the command and then type your message after that.");
		if (!args.message) return message.reply("It seems there's no mesage; type your message after the user.")
		try {
			await args.person.send(args.message)
			await message.react("👌")
		} catch (error) {
			await message.reply(error.message)
		}
	}
}