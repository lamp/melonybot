let { Command } = require("discord-akairo")
let { MessageAttachment } = require("discord.js")
let fs = require("fs").promises

module.exports = class extends Command {
	constructor() {
		super("ujel", {
			aliases: ["ujel"],
			description: "Posts a random troll face image and @s the person specified",
			args: [
				{
					id: "person",
					type: "user"
				}
			],
			typing: true
		}).usage = "<user>"
	}
	async exec(message, args) {
		let dirlist = await fs.readdir("images/trollfaces")
		let randomitem = dirlist[ Math.floor( Math.random() * dirlist.length ) ];
		await message.channel.send(String(args.person || message.author), new MessageAttachment(`images/trollfaces/${randomitem}`));
	}
}