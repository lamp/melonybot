module.exports = class extends Akairo.Command {
	constructor() {
		super("responders", {
			aliases: ["triggerwords"],
			description: "Shows a list of trigger words",
			typing: true
		})
	}
	async exec(message, args) {
		let responders = message.client.commandHandler.categories.get('responders').keyArray()
		responders = responders.map(x => `\`${x}\``)
		responders = responders.concat(message.client.triggerSystem.triggerMap.keyArray().map(x => x.map(x => `\`${x}\``).join(' or ')))
		responders = responders.join("\n")
		let embed = new Discord.MessageEmbed()
			.setTitle("Trigger words")
			.setDescription(responders)
			.setColor(THEME_COLOR)
		await message.channel.send(embed)
	}
}