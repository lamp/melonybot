module.exports = class extends Akairo.Command {
	constructor() {
		super("bruh", {
			aliases: ["bruh"],
			description: "Reacts to the last message with 🇧🇷🇺🇭"
		})
	}
	async exec(message, args) {
		let a = message.channel.messages.cache.array()
		let previousMsg = a[a.indexOf(message) - 1]
		if (!previousMsg) return message.react("⚠️")
		message.delete().catch(e => console.error("bruh delete", e.message))
		for (let b of "🇧🇷🇺🇭") previousMsg.react(b)
	}
}