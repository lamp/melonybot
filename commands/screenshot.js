let { Command } = require('discord-akairo');

module.exports = class extends Command {
    constructor() {
        super('screenshot', {
           aliases: ['screenshot'],
           args: [{
               id: "url",
               match: "content"
           }],
           description: "Screenshots of the provided website.",
           typing: true
        }).usage = "<url>"
    }

    async exec(message, args) {
        await message.react('🆗');
        let browser = await require.main.require("./browser")
        let page = await browser.newPage();
        let url = args.url;
        if (!(url.startsWith("http://") || url.startsWith("https://"))) url = "http://" + url;
        await page.goto(url);
        let screenshot = await page.screenshot({type: 'png'});
        await page.close();
        await message.channel.send({files:[{ attachment: screenshot, name: "screenshot.png" }]});
    }
}