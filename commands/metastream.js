let metastreamUrl;
module.exports = class extends Akairo.Command {
	constructor() {
		super("metastream", {
			aliases: ["metastream"],
			description: "Creates a new Metastream session",
			typing: true
		})
	}
	async exec(message, args) {
		if (metastreamUrl) return await message.channel.send(metastreamUrl)
		await message.channel.send("Creating a Metastream session, just a moment...")
		let browser = await require.main.require("./browser")
		let page = await browser.newPage()
		await page.goto("https://app.getmetastream.com")
		await page.evaluate("localStorage.clear()")
		let usernameInput = await page.waitForSelector("#profile_username")
		await usernameInput.type("Melony")
		await page.keyboard.press("Enter")
		let startSession = await page.waitForSelector(`a[href^="/join/"]`)
		await startSession.click()
		/*setInterval(async () => {
			let allowButton = await page.$('[title="Allow"]')
			if (allowButton) await allowButton.click()
		}, 2000)*/
		try {
			await (await page.waitForSelector(`[title="Settings"]`)).click()
			await (await page.$$(`[class^="UserAvatar__image"]`)).pop().click()
			await (await page.$x("//button[contains(text(), 'Session')]"))[0].click()
			await (await page.$x(`//span[contains(text(), 'Public')]`))[0].click()
			await (await page.$x(`//button[contains(text(), 'Advanced')]`))[0].click()
			await (await page.$(`label[for="safebrowse"]`)).click()
			await (await page.$(`button[class^="Modal__close"]`)).click()
		} catch(e) { console.error(e.stack) }
		await message.channel.send(`Here you go:\n${metastreamUrl = page.url()}`)
		page.on("close", () => metastreamUrl = undefined)
	}
}