let { Command } = require("discord-akairo")

module.exports = class extends Command {
	constructor() {
		super("ivy", {
			aliases: ["ivy"],
			description: "@’s the person with the discord id:(346862223135735808) and dms them with a heart and I love you message.",
			typing: true,
			ownerOnly: true
		})
	}
	async exec(message, args) {
		let ivy = await message.client.users.fetch('346862223135735808')
		if (ivy) {
			try {
				await ivy.send(`:heart: I love you`)
				await message.channel.send(ivy.toString())
			} catch (error) {
				await message.channel.send("I can't dm Ivy :cry:")
			}
		}
		else await message.reply("I can't find Ivy :cry:")

	}
}