var fs = require("fs").promises
module.exports = class extends Akairo.Command {
	constructor() {
		super("stock", {
			aliases: ["stock"],
			description: "Posts a random stock image.",
			typing: true
		})
	}
	async exec(message, args) {
		let dirlist = await fs.readdir("images/stock-images");
        let randomitem = dirlist[ Math.floor( Math.random() * dirlist.length ) ];
        await message.channel.send(new Discord.MessageAttachment(`images/stock-images/${randomitem}`));
	}
}