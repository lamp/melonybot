module.exports = class extends Akairo.Command {
	constructor() {
		super("ban", {
			aliases: ["ban"],
			args: [{
				id:"user",
				type:"user"
			}],
            userPermissions: ['BAN_MEMBERS'],
		}).usage = "<user>"
	}
	async exec(message, args) {
		try {
			await user.ban()
			await message.react("👌")
		} catch(e) {
			await message.reply(e.message)
		}
	}
}