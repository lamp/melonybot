const { Command } = require('discord-akairo');

class PingCommand extends Command {
    constructor() {
        super('ping', {
           aliases: ['ping'],
           description: "Pings a random person on the server.",
           ownerOnly: true,
           typing: true
        });
    }

    async exec(message) {
        await message.channel.send(String((await message.guild.members.fetch()).random()));
    }
}

module.exports = PingCommand;