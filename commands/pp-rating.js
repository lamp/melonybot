let { Command } = require("discord-akairo")

module.exports = class extends Command {
    constructor() {
        super("pp-rating", {
            aliases: ["pp-rating", "pp"],
            description: "Rates pp.",
            typing: true
        })
    }
    async exec(message, args) {
        if (!message.author.pprating)
            message.author.pprating = (String(Math.random() * 10)).substr(0,3);
        await message.reply(`I rate your pp a **${message.author.pprating}** out of 10.`);
    }
}