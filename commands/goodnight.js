const { Command } = require('discord-akairo');

module.exports = class extends Command {
    constructor() {
        super('goodnight', {
           aliases: ['goodnight', 'gn'],
           description: "Let the bot know you're going to bed and bot sends a heartwarming goodnight message.",
           typing: true
        });
    }

    exec(message) {
        return message.channel.send(`Goodnight ${message.member.displayName} :heart:`);
    }
}