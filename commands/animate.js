module.exports = class extends Akairo.Command {
	constructor() {
		super("animate", {
			aliases: ["animate"],
			description: "Animates text provided by the user.",
			args: [
				{
					id: "text",
					match: "content"
				}
			]
		}).usage = "<text...>"
	}
	async exec(message, args) {
		let text = args.text;
		if (!text) return await message.channel.send("animate what?")
		try { await message.delete() } catch(e) { console.error(`animate delete ${e.message}`) }
		let atext = text[0]
		let cursor = true
		let x = await message.channel.send(atext + '|')
		for (let i = 1; i < text.length; i++) {
			await new Promise(r => setTimeout(r, 500))
			let e = atext = atext + text[i]
			if (cursor = !cursor) e += `|`
			await x.edit(e)
		}
	}
}