let { Command } = require("discord-akairo")

module.exports = class extends Command {
    constructor() {
        super("eval", {
            aliases: ["eval", '>'],
            ownerOnly: true,
            args: [
                {
                    id: "code",
                    match: "content"
                }
            ],
            category: "admin",
            description: "Evaluates javascript",
            typing: true
        }).usage = "<code...>"
    }
    async exec(message, args) {
        //with (message) {
            var x = await eval(args.code)
        //}
        x = require('util').inspect(x, {depth: 1})
        await message.channel.send(`\`\`\`js\n${x}\`\`\``, {split:{maxLength:2000,prepend:'```js\n',append:'```'}})
    }
}