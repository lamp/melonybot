var {JSDOM} = require('jsdom')
let dom, requestDate;
module.exports = class extends Akairo.Command {
	constructor() {
		super("recommendation", {
			aliases: ["recommendation"],
			description: "Recommends anime according to Myanimelist..",
			typing: true
		})
	}
	async exec(message, args) {
		if (!dom || new Date() - requestDate > 6e5) {
			let request = await (await fetch('https://myanimelist.net/recommendations.php?s=recentrecs&t=anime')).text()
			requestDate = new Date()
			dom = new JSDOM(request)
		}

		let recs = dom.window.document.getElementById("content").querySelector(`div[style="padding: 0 20px;"]`).children
		let randomRec = recs[ Math.floor( Math.random() * recs.length ) ]

		let ifYouLikedThis_a = randomRec.getElementsByTagName('a')[1]
		let ifYouLikedThis_title = ifYouLikedThis_a.children[0].innerHTML
		let ifYouLikedThis_url = ifYouLikedThis_a.href
		let thenYouMightLike_a = randomRec.getElementsByTagName('a')[4]
		let thenYouMightLike_title = thenYouMightLike_a.children[0].innerHTML
		let thenYouMightLike_url = "https://myanimelist.net" + thenYouMightLike_a.href
		let recText = randomRec.getElementsByClassName('recommendations-user-recs-text')[0].innerHTML
		let animeRecBy_name = randomRec.getElementsByTagName('a')[7].innerHTML
		let animeRecBy_url = "https://myanimelist.net" + randomRec.getElementsByTagName('a')[7].href
		let animeRecBy_when = randomRec.getElementsByTagName('a')[7].parentElement.innerHTML.split(" - ").pop()

		await message.channel.send(`If you liked this...\n${ifYouLikedThis_url}`)
		await message.channel.send(`Then you might like...\n${thenYouMightLike_url}`)
		await message.channel.send(recText, new Discord.MessageEmbed().setDescription(`Anime rec by [${animeRecBy_name}](${animeRecBy_url}) - ${animeRecBy_when}`))

	}
}