let { Command } = require("discord-akairo")
let { MessageAttachment } = require('discord.js')
let fetch = require('node-fetch')
let parseXml = require('xml2js').parseStringPromise

let xxxCache = [];

module.exports = class extends Command {
	constructor() {
		super("nsfw", {
			aliases: ["nsfw"],
			description: "Sends a nsfw image of Melony",
			typing: true
		})
	}
	async exec(message, args) {
		if (!xxxCache.length) {
			await (async function getNextPage(page = 0){
				console.log(`downloading melony pron index page ${page}`)
				let xxx = await fetch(`https://rule34.xxx/index.php?page=dapi&s=post&q=index&tags=melony_(pokemon)&pid=${page}`)
				xxx = await parseXml(await xxx.text())
				if (!xxx.posts.post) return;
				xxx = xxx.posts.post.map(x => x.$.file_url)
				xxxCache = xxxCache.concat(xxx)
				await getNextPage(++page)
			})()
			console.log(`finished with ${xxxCache.length} melony prons`)
		}
		let randomx = xxxCache[ Math.floor( Math.random() * xxxCache.length ) ]
		//console.log(randomx)
		await message.channel.send(new MessageAttachment(randomx))
	}
}