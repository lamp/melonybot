let { MessageAttachment } = require('discord.js');
let { Command } = require('discord-akairo');
let fs = require('fs').promises;

module.exports = class extends Command {
    constructor() {
        super('melonyv2', {
           aliases: ['melony'] ,
           regex: /^melony$/i,
           description: "Sends a random image of Melony.",
           typing: true
        });
    }
    async exec(message) {
        let dirlist = await fs.readdir("images/melonies");
        let randomitem = dirlist[ Math.floor( Math.random() * dirlist.length ) ];
        await message.channel.send(new MessageAttachment(`images/melonies/${randomitem}`));
    }
}
