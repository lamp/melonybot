const { Command } = require('discord-akairo');

module.exports = class extends Command {
    constructor() {
        super('headpat', {
           aliases: ['headpat','hp'],
           description: "Headpats.",
           typing: true
        });
    }

    exec(message) {
        return message.reply('*headpats*');
    }
}
