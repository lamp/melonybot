let { MessageEmbed } = require("discord.js")
let { Command } = require("discord-akairo")

module.exports = class extends Command {
	constructor() {
		super("help", {
			aliases: ["help", "commands"],
			description: "Help",
			typing: true
		})
	}
	async exec(message, args) {
		let commands = message.client.commandHandler.categories.get("default")
		let list = commands.map(command => {
			let faliases = command.aliases.map(alias => 
				`\`${message.client.commandHandler.prefix}${alias}\``
			)
			faliases = faliases.join(' / ')
			return faliases + (command.usage ? ` \`${command.usage}\` ` : '') + (command.description ? ` — ${command.description}` : '')
		}).join('\n')
		let embed = new MessageEmbed()
			.setTitle("Commands")
			.setDescription(list)
			.setColor(THEME_COLOR)
		await message.channel.send(embed)
	}
}