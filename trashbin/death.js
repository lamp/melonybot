module.exports = class extends Akairo.Command {
	constructor() {
		super("death", {
			regex: /^death$/i,
			description: "Bot responds with https://www.youtube.com/watch?v=iHLMnP7bpnk",
			category: "responders"
		})
	}
	async exec(message, args) {
		await message.channel.send("https://www.youtube.com/watch?v=iHLMnP7bpnk")
	}
}