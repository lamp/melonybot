var {JSDOM} = require('jsdom')
var fs = require("fs")

var dom = new JSDOM(fs.readFileSync("a.txt","utf8"))

let recs = dom.window.document.getElementById("content").querySelector(`div[style="padding: 0 20px;"]`).children
let randomRec = recs[ Math.floor( Math.random() * recs.length ) ]

let ifYouLikedThis_a = randomRec.getElementsByTagName('a')[1]
let ifYouLikedThis_title = ifYouLikedThis_a.children[0].innerHTML
let ifYouLikedThis_url = ifYouLikedThis_a.href
let thenYouMightLike_a = randomRec.getElementsByTagName('a')[4]
let thenYouMightLike_title = thenYouMightLike_a.children[0].innerHTML
let thenYouMightLike_url = "https://myanimelist.net" + thenYouMightLike_a.href
let recText = randomRec.getElementsByClassName('recommendations-user-recs-text')[0].innerHTML
let animeRecBy_name = randomRec.getElementsByTagName('a')[7].innerHTML
let animeRecBy_url = "https://myanimelist.net" + randomRec.getElementsByTagName('a')[7].href
let animeRecBy_when = randomRec.getElementsByTagName('a')[7].parentElement.innerHTML.split(" - ").pop()

console.log({ifYouLikedThis_title, ifYouLikedThis_url, thenYouMightLike_title, thenYouMightLike_url, recText, animeRecBy_name, animeRecBy_url, animeRecBy_when})
