(async function() {
	let browser = await require("puppeteer").launch({headless:false, args: [
		`--load-extension=` + require("path").join(process.cwd(), `resources/metastream-extension-0.4.0_0`)
	], ignoreDefaultArgs: [
		"--disable-extensions"
	]})
	let page = await browser.newPage()
	await page.goto("https://app.getmetastream.com")
	let usernameInput = await page.waitForSelector("#profile_username")
	await usernameInput.type("Melony")
	await page.keyboard.press("Enter")
	let startSession = await page.waitForSelector(`a[href^="/join/"]`)
	await startSession.click()
	/*setInterval(async () => {
		let allowButton = await page.$('[title="Allow"]')
		if (allowButton) await allowButton.click()
	}, 2000)*/
	try {
		await (await page.waitForSelector(`[title="Settings"]`)).click()
		await (await page.$$(`[class^="UserAvatar__image"]`)).pop().click()
		await (await page.$x("//button[contains(text(), 'Session')]"))[0].click()
		await (await page.$x(`//span[contains(text(), 'Public')]`))[0].click()
		await (await page.$x(`//button[contains(text(), 'Advanced')]`))[0].click()
		await (await page.$(`label[for="safebrowse"]`)).click()
		await (await page.$(`button[class^="Modal__close"]`)).click()
	} catch(e) { console.error(e.stack) }
	console.log(page.url())
})()
