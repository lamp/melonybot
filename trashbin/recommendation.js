
let requestCache, requestDate;

module.exports = class extends Akairo.Command {
	constructor() {
		super("recommendation", {
			aliases: ["recommendation"],
			description: "Recommends anime according to Myanimelist..",
			typing: true
		})
	}
	async exec(message, args) {
		if (!requestCache || new Date() - requestDate > (requestCache.request_cache_expiry || 1800) * 1000) {
			//TODO this is getting recommendations from one particular anime
			requestCache = await fetch('https://api.jikan.moe/v3/anime/1/recommendations')
				.then(res => res.json())
			requestDate = new Date()
		}

		let rr = requestCache.recommendations[ Math.floor( Math.random() * requestCache.recommendations.length ) ]
		let embed = new Discord.MessageEmbed()
			.setColor(THEME_COLOR)
			.setTitle(rr.title)
			.setImage(rr.image_url)
			.setURL(rr.recommendation_url)
			.setFooter(`${rr.recommendation_count} recommendations`)
		await message.channel.send(embed)
		
	}
}