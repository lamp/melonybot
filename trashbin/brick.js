let { Command } = require("discord-akairo")

module.exports = class extends Command {
	constructor() {
		super("brick", {
			regex: /^brick$/i,
			description: "template command",
			category: 'responders',
			description: "Responds with brick emoji"
		})
	}
	async exec(message, args) {
		await message.channel.send(":bricks:")
	}
}