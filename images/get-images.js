#!/usr/bin/env node
if (process.argv.length < 4) { console.log("Usage: node get-images.js <directory> <size> <keywords...>"); process.exit(1) }
var rimraf = require("rimraf").sync;
var GIScraper = require("images-scraper");
var fetch = require("node-fetch");
var fs = require("fs");

(async function(){
	rimraf(process.argv[2]+"/*");
	var gi = new GIScraper({ puppeteer: { headless: false } });
	console.log(Number(process.argv[3]))
	let results = await gi.scrape(process.argv.slice(4).join(' '), Number(process.argv[3]));
	console.log(`${results.length} results`);
	await Promise.all(results.map(async (result, number) => {
		let res = await fetch(result.url);
		let type = res.headers.get('content-type');
		if (res.status != '200') return console.log(`${number} not-ok status ${res.status}`);
		if (!['image/png','image/jpeg','image/jpg','image/gif'].includes(type)) {
			return console.log(`${number} invalid type ${type}`);
		}
		let downloadpath = `${process.argv[2]}/${number}.${type.split('/').pop()}`;
		res.body.pipe(fs.createWriteStream(downloadpath));
		await new Promise((resolve,reject) => {
			res.body.on('end', resolve);
			res.body.on('error', reject);
		});
		console.log(`downloaded ${downloadpath}`)
	}));
	console.log("done");
})().catch(error => {console.error(error) && process.exit(1)});


