let fs = require("fs")
let parseCsv = require('csv-parse/lib/sync')
let { Collection } = require("discord.js")

class SimpleTriggerSystem {
	constructor(client, pathToCsv) {
		// parsing a csv would've been super easy, except when you want to allow commas to be escaped..
		let x = parseCsv(fs.readFileSync(pathToCsv, "utf8"), {
			//comment: "//", // would've used # but that might be used to reference a discord channel
			// ah shit urls have those
			// ah what the hell we'll do without them
		})
		for (let z of x) z[0] = z[0].split('|')
		this.triggerMap = new Collection(x)
		client.on("message", message => {
			if (message.author.bot) return;
			for (let [triggers, response] of this.triggerMap) {
				if (triggers.includes(message.content.toLowerCase())) message.channel.send(response)
			}
		})
	}
}

module.exports = SimpleTriggerSystem