module.exports = require("puppeteer").launch({headless:false, args: [
	`--load-extension=` + require("path").join(process.cwd(), `resources/metastream-extension-0.4.0_0`)
], ignoreDefaultArgs: [
	"--disable-extensions"
]})

module.exports.then(browser => {
	browser.on("disconnected", () => {
		delete require.cache[module.id]
	})
})